// After you installed all the required modules you can start using react with material-ui and style our components.
/** @jsxImportSource @emotion/react */
import React from "react";
import { Content } from "./Components/Content";
import { Header } from "./Components/Header";

export const App = () => {
	return (
		<>
			<Header />
			<Content />
		</>
	);
};
