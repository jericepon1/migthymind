export const Classes = [
	{
		code: '12ENGA',
		year: 12,
		name: 'English',
		colorcode: 'green',
		count: 3
	},
	{
		code: '12ENGB',
		year: 12,
		name: 'English',
		colorcode: 'blue',
		count: 0
	},
	{
		code: '08MATHS',
		year: 8,
		name: 'Math',
		colorcode: 'yellow',
		count: 3
	},
	{
		code: '09SCI',
		year: 9,
		name: 'Science',
		colorcode: 'pink',
		count: 6
	},
	{
		code: '09HASS',
		year: 9,
		name: 'Humanities and Social',
		colorcode: 'purple',
		count: 6
	},
]
