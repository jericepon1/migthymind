import { CardMedia, makeStyles, Typography } from "@material-ui/core";
import { Button, Card, CardContent, Link } from "@mui/material";
import React from "react";
import Figure from "../Images/Figure.png";

const useStyles = makeStyles(({ breakpoints, palette }) => ({
	Explore: {
		[breakpoints.up("laptop")]: {
			marginLeft: "29px",
		},
		overflow: "initial!important",
		backgroundColor: "#D2DFFF!important",
		border: "0!important",
	},
	ExploreFigure: {
		marginBottom: "13px",
		[breakpoints.up("lg")]: {
			display: "inline-block",
		},
		"@media (min-width: 900px)": {
			marginTop: "-17.4%",
		},
	},
	ExploreTitle: {
		fontSize: "1.125rem",
		marginBottom: "7px",
		alignSelf: "flex-start",
	},
	ExploreBody: {
		fontSize: "15px",
		marginBottom: "22px",
		lineHeight: "19px",
	},
	ExploreButton: {
		fontSize: "15px",
		marginBottom: "22px",
		textTransform: "inherit!important",
		lineHeight: "28px",
	},
	ExploreLink: {
		marginTop: "14px!important",
		fontSize: "15px",
	},
}));
export const ExploreCard = (props) => {
	const classes = useStyles(props);
	return (
		<Card className={classes.Explore} variant="outlined">
			<CardContent
				sx={{
					justifyContent: "center",
					alignItems: "center",
					display: "flex",
					flexDirection: "column",
				}}
			>
				<CardMedia
					className={classes.ExploreFigure}
					image={Figure}
					component="img"
				/>
				<Typography className={classes.ExploreTitle} variant="h6">
					Explore your new portal
				</Typography>
				<Typography className={classes.ExploreBody} variant="body2">
					Improved class focus, unit plans, live sessions, additional
					tracking, features and much more.
				</Typography>
				<Button
					className={classes.ExploreButton}
					fullWidth
					disableElevation
					variant="contained"
				>
					See how it works
				</Button>
				<Link className={classes.ExploreLink} href="/">
					Getting started guide
				</Link>
			</CardContent>
		</Card>
	);
};
