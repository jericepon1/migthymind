import {
	Typography,
	Container,
	Box,
	Link,
	makeStyles,
	Button,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { Grid, Toolbar } from "@mui/material";
import React from "react";
import { ClassCard } from "./ClassCard";
import { Classes } from "../Data";
import { ExploreCard } from "./ExploreCard";
import { HelpCard } from "./HelpCard";

const useStyles = makeStyles(({ breakpoints, palette }) => ({
	ClassList: {
		marginTop: 40,
		marginBottom: 40,
		[breakpoints.up("laptop")]: {
			padding: "0 5px",
		},
	},
	ClassListTitle: {},
	ClassCardList: {
		[breakpoints.up("laptop")]: {
			// padding: "0 5px",
		},
	},
	ClassListTextHeader: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "flex-start!important",

		[breakpoints.up("laptop")]: {
			justifyContent: "space-between",
			flexDirection: "row",
			alignItems: "flex-end!important",
		},
	},
}));

export const Content = () => {
	const classes = useStyles();
	return (
		<Container fixed className={classes.ClassList}>
			<Grid
				container
				columnSpacing={3}
				columns={12}
				sx={{ marginBottom: "30px" }}
			>
				<Grid item lg={9} md={8} sm={12} xs={12}>
					<Toolbar
						className={classes.ClassListTextHeader}
						disableGutters
					>
						<Box
							sx={{
								display: "flex",
								flexWrap: "wrap",
								flexDirection: "column",
							}}
						>
							<Typography variant="h5">
								Here are your classes
							</Typography>
							<Typography variant="body1">
								Select a class to view this week's assigned
								activities and begin your lesson.
							</Typography>
						</Box>

						<Link href="#" color="textSecondary">
							View all classes
						</Link>
					</Toolbar>
				</Grid>
			</Grid>
			<Grid container columnSpacing={3} columns={12}>
				<Grid item xs={12} md={8} lg={9}>
					<Grid
						className={classes.ClassCardList}
						container
						columns={12}
						columnSpacing={3}
					>
						{Classes.map((classdetail) => (
							<Grid
								item
								xs={12}
								md={6}
								lg={4}
								xl={4}
								sx={{ paddingBottom: "20px" }}
								key={classdetail.code}
							>
								<ClassCard classDetail={classdetail} />
							</Grid>
						))}
						<Grid
							item
							xs={12}
							md={6}
							lg={4}
							sx={{
								opacity: "0.3",
								paddingBottom: "20px",
								display: "flex",
								justifyContent: "stretch",
								alignItems: "stretch",
							}}
						>
							<Box
								sx={{
									width: "100%",
									display: "flex",
									justifyContent: "stretch",
									backgroundImage: 'url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSl9HNyKV_U9w0IvWKy-1TVpgm1xIpy9jLZaw&usqp=CAU")',
									backgroundRepeat: "no-repeat",
									backgroundSize: '104% 109%',
									backgroundPosition: 'center',
								}}
							>
								<Button size="large" fullWidth>
									<Add fontSize="large" />
								</Button>
							</Box>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xs={12} md={4} lg={3}>
					<ExploreCard />
					<HelpCard />
				</Grid>
			</Grid>
		</Container>
	);
};
