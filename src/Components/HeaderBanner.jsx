import React from "react";
import {
	Container,
	Box,
	Toolbar,
	Typography,
	Button,
	Menu,
	MenuItem,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Add, KeyboardArrowDown } from "@material-ui/icons";


const useStyles = makeStyles(({ breakpoints, palette }) => ({
	HeaderBanner: {
		width: "100%",
		display: "flex",
		minHeight: "187px",
		alignItems: "flex-start",
		justifyContent: "center",
		flexDirection: "column",
		padding: "38px 15px",

		[breakpoints.up("laptop")]: {
			alignItems: "center",
			justifyContent: "space-between",
			flexDirection: "row",
			padding: "38px 8px",
		},
	},
	HeaderBannerTitle: {
		marginBottom: 27,
	},
	HeaderBannerSubTitle: {
		textTransform: "uppercase",
		color: palette.common.muted,
		fontWeight: "bold",
		marginBottom: "5px",
	},
	SummaryDetails: {
		minHeight: "auto",
		padding: 0,
		display: "flex",
		flexWrap: "wrap",

		[breakpoints.up("laptop")]: {
			marginBottom: 0,
		},
	},
	SummaryDetailItem: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		marginTop: "20px",
		"&:not(:last-child)": {
			marginRight: 20,
		},
		"&::before": {
			content: "''",
			width: 9,
			height: 9,
			borderRadius: "50%",
			display: "block",
			marginRight: 6,
			backgroundColor: (props) => {
				if (props.markercolor === "error") return palette.error.main;
				if (props.markercolor === "primary")
					return palette.primary.main;
				if (props.markercolor === "success")
					return palette.success.main;
			},
		},
		[breakpoints.up("laptop")]: {
			marginTop: 0,
		},
	},
	ActionButtons: {
		padding: 0,
		"&>button": {
			marginTop: 20,
			[breakpoints.up("laptop")]: {
				marginTop: 0,
			},
		},
		"&>button:not(:last-child)": {
			marginRight: 17,
		},
		display: "flex",
		flexWrap: "wrap",
	},
	ActionButtonDropdown: {
		padding: "6px 6px",
		minWidth: 36,
		marginLeft: 2,
		marginTop: "20px",
		[breakpoints.up("laptop")]: {
			marginTop: 0,
		},
	},
	ActionButtonActivity: {
		marginTop: "20px",
		[breakpoints.up("laptop")]: {
			marginTop: 0,
		},
	},
}));

const SummaryItem = (props) => {
	const classes = useStyles(props);

	return (
		<Typography variant="body1" className={classes.SummaryDetailItem}>
			{props?.text}
		</Typography>
	);
};

const SummaryDetails = () => {
	const classes = useStyles();

	return (
		<Toolbar className={classes.SummaryDetails}>
			<SummaryItem markercolor="primary" text="Due this week: 330" />
			<SummaryItem markercolor="success" text="Completed: 240" />
			<SummaryItem markercolor="error" text="Overdue: 33" />
		</Toolbar>
	);
};

const ButtonMenu = () => {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const open = Boolean(anchorEl);
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};
	const classes = useStyles();

	return (
		<>
			<Button
				id="fade-button"
				aria-controls="fade-menu"
				aria-haspopup="true"
				aria-expanded={open ? "true" : undefined}
				onClick={handleClick}
				variant="contained"
				color="primary"
				className={classes.ActionButtonDropdown}
			>
				<KeyboardArrowDown />
			</Button>
			<Menu
				id="fade-menu"
				MenuListProps={{
					"aria-labelledby": "fade-button",
				}}
				anchorEl={anchorEl}
				open={open}
				onClose={handleClose}
			>
				<MenuItem onClick={handleClose}>Option 1</MenuItem>
				<MenuItem onClick={handleClose}>Option 2</MenuItem>
				<MenuItem onClick={handleClose}>Option 3</MenuItem>
			</Menu>
		</>
	);
};

export const HeaderBanner = () => {
	const classes = useStyles();

	return (
		<Container className={classes.HeaderBanner} fixed>
			<Box>
				<Typography className={classes.HeaderBannerTitle} variant="h4">
					Welcome back, Jasmine
				</Typography>
				<Typography
					className={classes.HeaderBannerSubTitle}
					variant="body2"
				>
					Week 4 activity summary
				</Typography>
				<SummaryDetails />
			</Box>
			<Box>
				<Toolbar className={classes.ActionButtons}>
					<Button variant="outlined" color="secondary">
						My Calendar
					</Button>
					<Button variant="outlined" color="secondary">
						Weekly Report
					</Button>

					<Box sx={{ display: "flex" }}>
						<Button
							variant="contained"
							color="primary"
							startIcon={<Add />}
							className={classes.ActionButtonActivity}
						>
							Assign Activity
						</Button>
						<ButtonMenu />
					</Box>
				</Toolbar>
			</Box>
		</Container>
	);
};
