import React from "react";
import {
	Box,
	Button,
	Link,
	makeStyles,
	Toolbar,
	Typography,
} from "@material-ui/core";
import HelpIcon from "@material-ui/icons/Help";
import LogoDesktop from "../Images/Logo.png";
import NavDrawer from "./NavDrawer";

const useStyles = makeStyles(({ breakpoints, palette }) => ({
	Toolbar: {
		[breakpoints.up("tablet")]: {
			minHeight: 64,
		},
	},
	ToolbarLeft: {
		display: "none",

		[breakpoints.up("tablet")]: {
			display: "flex",
			paddingLeft: "21px",
			paddingRight: "21px",
		},
	},
	ToolbarRight: {
		marginLeft: "auto",
		display: "none",
		[breakpoints.up("tablet")]: {
			display: "flex",
			paddingLeft: "21px",
			paddingRight: "29px",
		},
	},
	HelpCentre: {
		textTransform: "initial",
		color: palette.common.muted,
		borderRight: `1px solid ${palette.common.muted}`,
		borderRadius: "0",
		marginRight: "25px",
		[breakpoints.up("laptop")]: {
			paddingRight: "31px",
		},
	},
	HelpCentreText: {
		display: "none",
		[breakpoints.up("laptop")]: {
			display: "inline-block",
		},
	},
	Nav: {
		color: "#fff",
		boxShadow: "0 20px 50px 10px rgb(8 18 38 / 47%)",
		minHeight: 54,
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",

		[breakpoints.up("laptop")]: {
			minHeight: 64,
			justifyContent: "flex-start",
		},
	},
	NavBrand: {
		display: "flex",
		alignItems: "center",
		marginRight: "9px",
		marginLeft: "19px",
		paddingRight: "20px",

		[breakpoints.up("laptop")]: {
			borderRight: `1px solid ${palette.common.muted}`,
		},
	},
	NavBrandDesktop: {
		// display: "none",

		// [breakpoints.up("laptop")]: {
		// 	display: "block",
		// },
	},
	NavBrandMobile: {
		display: "block",

		[breakpoints.up("laptop")]: {
			display: "none",
		},
	},
	NavLink: {
		color: "#fff",
		"&:not(:last-child)": {
			marginRight: 31,
		},
		"&.active": {
			position: "relative",
			"&::after": {
				content: "''",
				borderTop: `1px solid ${palette.primary.main}`,
				width: "100%",
				position: "absolute",
				right: "0",
				bottom: "-8px",
				left: "0",
			},
		},
		"&:not(.active)": {
			color: palette.common.muted,
		},
	},
	AccountName: {
		color: "#fff",
	},
	AccountType: {
		color: palette.common.muted,
	},
	AccountAvatar: {
		display: "flex",
		alignItems: "center",
		"&>img": {
			borderRadius: 5,
			width: 30,
		},
	},
	AvatarDetails: {
		textAlign: "right",
		marginRight: 10,
		display: "none",
		[breakpoints.up("laptop")]: {
			display: "flex",
		},
		"&>a, &>p": {
			lineHeight: "14px",
		},
	},
	Menu: {
		marginLeft: "auto",
	},
}));

export const Nav = () => {
	const classes = useStyles();

	return (
		<Box className={classes.Nav}>
			<Link href="/" underline="none" className={classes.NavBrand}>
				<img
					className={classes.NavBrandDesktop}
					src={LogoDesktop}
					alt=""
					srcSet=""
				/>
			</Link>
			<NavDrawer />
			<Toolbar className={`${classes.ToolbarLeft} ${classes.Toolbar}`}>
				<Link
					href="/"
					underline="none"
					className={`${classes.NavLink} active`}
				>
					<Typography variant="body2">Home</Typography>
				</Link>
				<Link href="/" underline="none" className={classes.NavLink}>
					<Typography variant="body2">Classes</Typography>
				</Link>
				<Link href="/" underline="none" className={classes.NavLink}>
					<Typography variant="body2">Planner</Typography>
				</Link>
				<Link href="/" underline="none" className={classes.NavLink}>
					<Typography variant="body2">School Data</Typography>
				</Link>
				<Link href="/" underline="none" className={classes.NavLink}>
					<Typography variant="body2">Library</Typography>
				</Link>
			</Toolbar>
			<Toolbar className={`${classes.ToolbarRight} ${classes.Toolbar}`}>
				<Button
					className={classes.HelpCentre}
					disableRipple
					variant="text"
					color="default"
					startIcon={<HelpIcon />}
				>
					<span className={classes.HelpCentreText}>Help Centre</span>
				</Button>
				<Box
					sx={{
						display: "flex",
						flexDirection: "row",
						justifyContent: "center",
					}}
				>
					<Box
						className={classes.AvatarDetails}
						sx={{
							display: "flex",
							flexDirection: "column",
							justifyContent: "flex-end",
						}}
					>
						<Link href="/" underline="none">
							<Typography
								variant="body2"
								className={classes.AccountName}
							>
								Jasmine Finn
							</Typography>
						</Link>
						<Typography
							variant="body2"
							color="textSecondary"
							className={classes.AccountType}
						>
							Teacher Account
						</Typography>
					</Box>
					<Link
						href="/"
						underline="none"
						className={`${classes.NavLink} ${classes.AccountAvatar}`}
					>
						<img
							src="https://ui-avatars.com/api/?size=30&background=8660F1&color=fff&font-size=0.6&name=Jasmin+Finn"
							alt=""
							srcSet=""
						/>
					</Link>
				</Box>
			</Toolbar>
		</Box>
	);
};
