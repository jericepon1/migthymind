import { List, makeStyles, Typography } from "@material-ui/core";
import { Card, Divider, ListItem, ListItemButton } from "@mui/material";
import HelpIcon from "@material-ui/icons/Help";

import React from "react";
import { ArrowForward } from "@material-ui/icons";

const useStyles = makeStyles(({ breakpoints, palette }) => ({
	Help: {
		[breakpoints.up("laptop")]: {
			marginLeft: "29px",
		},
		overflow: "initial!important",
		marginTop: "18px",
	},
	HelpTitle: {
		fontSize: "1rem",
		marginBottom: "7px",
		alignSelf: "flex-start",
		display: "flex",
		alignItems: "center",
		fontWeight: "bold",
		"&>svg": {
			width: "18px",
			height: "18px",
			marginRight: "9px",
			color: "#9FABC4",
		},
	},
	HelpLink: {
		marginTop: "14px!important",
		fontSize: "15px",
	},
	ListItemButton: {
		"&>svg": {
			color: "#9FABC4",
		},
	},
}));
export const HelpCard = (props) => {
	const classes = useStyles(props);
	return (
		<Card className={classes.Help} variant="outlined">
			<List>
				<ListItem>
					<Typography className={classes.HelpTitle} variant="body1">
						<HelpIcon />
						Help & support
					</Typography>
				</ListItem>
				<Divider variant="middle" sx={{ marginTop: "8px", marginBottom: "10px" }} />
				{[
					"Visite help centre",
					"Send us your feedback",
					"Make a request or suggestions",
					"Report an issue",
				].map((item) => (
					<ListItem disableGutters disablePadding alignItems="flex-start" key={item}>
						<ListItemButton className={classes.ListItemButton}>
							<ArrowForward fontSize="small" />
							<Typography sx={{ marginLeft: 16 }} variant="body2">
								{item}
							</Typography>
						</ListItemButton>
					</ListItem>
				))}
				<Divider variant="fullWidth" sx={{ margin: "10px 0" }} />
				{["Teacher support group", "Schedule a consultation"].map(
					(item) => (
						<ListItem
							disableGutters
							disablePadding
							alignItems="flex-start"
							key={item}
						>
							<ListItemButton className={classes.ListItemButton}>
								<ArrowForward />
								<Typography
									sx={{ marginLeft: 16 }}
									variant="body2"
								>
									{item}
								</Typography>
							</ListItemButton>
						</ListItem>
					)
				)}
			</List>
		</Card>
	);
};
