import React from "react";
import {
	alpha,
	Box,
	Button,
	ButtonGroup,
	createTheme,
	Divider,
	makeStyles,
} from "@material-ui/core";
import { Add, Star } from "@mui/icons-material";
import { Card, CardActions, CardContent, Typography } from "@mui/material";
import { OfflineBolt } from "@material-ui/icons";
const theme = createTheme();

const useStyles = makeStyles(({ palette }) => ({
	ClassCard: {
		borderTopWidth: 3,
		borderWidth: 1,
		borderStyle: "solid",
		borderColor: "rgba(0, 0, 0, 0.12)",
		borderTopColor: handleColor(palette),
		boxShadow: "none!important",
	},
	ClassCardContent: {
		background: handleColorGradient(palette),
	},
	ClassCode: {
		"&>h5": {
			fontWeight: "bold",
			marginLeft: 15,
		},
		color: {},
	},
	ClassCodeIcon: {
		backgroundColor: handleColor(palette),
		width: "20px",
		height: "20px",
		borderRadius: "5px",
		padding: "1px",

		"&>svg": {
			color: "white",
		},
	},
	ClassCodeDetails: {
		color: palette.common.muted,
		"&>button": {
			padding: "0 10px",
			minWidth: "auto",
			lineHeight: "initial",
			"&:first-child": {
				paddingLeft: 0,
			},
			"&:last-child": {
				paddingRight: 0,
			},
		},
	},
}));

const ClassSummarDetailItem = ({ text, badge, count }) => {
	return (
		<Box
			sx={{
				display: "flex",
				flexDirection: "row",
				justifyContent: "space-between",
				paddingTop: "10px",
			}}
		>
			<Typography>{text}</Typography>
			{badge && (
				<Box
					sx={{
						backgroundColor: "rgb(236, 238, 242)",
						borderRadius: 5,
						minWidth: "43px",
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					{count}
				</Box>
			)}
		</Box>
	);
};

export const ClassCard = (props) => {
	const classes = useStyles(props);

	return (
		<Card className={classes.ClassCard}>
			<CardContent
				className={classes.ClassCardContent}
				sx={{ padding: "15px 15px 10px 20px" }}
			>
				<Box
					sx={{ display: "flex", alignItems: "center" }}
					className={classes.ClassCode}
				>
					<Box
						className={classes.ClassCodeIcon}
						xs={{
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
						}}
					>
						<OfflineBolt fontSize="small" />
					</Box>
					<Typography variant="h5">
						{props.classDetail.code}
					</Typography>
				</Box>
				<ButtonGroup
					size="medium"
					variant="text"
					disableElevation
					className={classes.ClassCodeDetails}
				>
					<Button>
						<Star fontSize="small" color="primary" />
					</Button>
					<Button>Year {props.classDetail.year}</Button>
					<Button>
						<Typography variant="inherit" noWrap>
							{props.classDetail.name}
						</Typography>
					</Button>
				</ButtonGroup>

				<Box>
					<ClassSummarDetailItem
						text="Activities due this week"
						badge={true}
						count={props.classDetail.count}
					/>
					<ClassSummarDetailItem
						text="Assign activities"
						badge={false}
					/>
					<ClassSummarDetailItem
						text="Class calendar"
						badge={false}
					/>
				</Box>
			</CardContent>
			<Divider variant="middle" />
			<CardActions>
				<Button
					sx={{ color: theme.palette.common.muted }}
					size="small"
					variant="text"
					startIcon={<Add />}
				>
					Add student
				</Button>
			</CardActions>
		</Card>
	);
};

const handleColor = (palette) => {
	return (props) => {
		if (props.classDetail.colorcode === "blue") return palette.primary.main;
		if (props.classDetail.colorcode === "green")
			return palette.success.main;
		if (props.classDetail.colorcode === "yellow")
			return palette.warning.main;
		if (props.classDetail.colorcode === "purple")
			return palette.secondary.dark;
		if (props.classDetail.colorcode === "pink")
			return palette.secondary.light;
	};
};
const handleColorGradient = (palette) => {
	return (props) => {
		if (props.classDetail.colorcode === "blue")
			return `linear-gradient(to bottom, ${alpha(
				palette.primary.main,
				0.2
			)} 0px, #fff 65px)`;
		if (props.classDetail.colorcode === "green")
			return `linear-gradient(to bottom, ${alpha( palette.success.main, 0.2 )} 0px, #fff 65px)`;
		if (props.classDetail.colorcode === "yellow")
			return `linear-gradient(to bottom, ${alpha( palette.success.main, 0.2 )} 0px, #fff 65px)`;
		if (props.classDetail.colorcode === "purple")
			return `linear-gradient(to bottom, ${alpha(
				palette.secondary.dark,
				0.2
			)} 0px, #fff 65px)`;
		if (props.classDetail.colorcode === "pink")
			return `linear-gradient(to bottom, ${alpha(
				palette.secondary.light,
				0.2
			)} 0px, #fff 65px)`;
	};
};
