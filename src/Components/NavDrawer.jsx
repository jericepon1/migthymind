import {
	Box,
	Divider,
	Drawer,
	IconButton,
	List,
	ListItemIcon,
	ListItemText,
	makeStyles,
	Typography,
} from "@material-ui/core";
import { ListItemButton, ListItem } from "@mui/material";
import { Help, Menu } from "@material-ui/icons";

import React, { useState } from "react";

const useStyles = makeStyles(({ breakpoints }) => ({
	NavDrawer: {
		display: "block",
		[breakpoints.up("tablet")]: {
			display: "none",
		},
	},
}));

const NavDrawer = () => {
	const [open, setOpen] = useState(false);
	const classes = useStyles();
	const handleClose = () => {
		setOpen((open) => (open = !open));
	};

	return (
		<Box className={classes.NavDrawer}>
			<IconButton
				onClick={() => handleClose()}
				color="secondary"
				children={<Menu />}
			/>
			<Drawer
				variant="temporary"
				anchor="left"
				open={open}
				onClose={() => {
					handleClose();
				}}
			>
				<Box
					sx={{
						width: "70vw",
						bgcolor: "background.paper",
					}}
				>
					<nav aria-label="main mailbox folders">
						<List dense>
							{[
								"Home",
								"Classes",
								"Planner",
								"School Data",
								"Library",
							].map((item) => (
								<ListItem disablePadding key={item}>
									<ListItemButton>
										<Typography variant="body1">
											{item}
										</Typography>
									</ListItemButton>
								</ListItem>
							))}
							<Divider variant="middle" />
							<ListItem disablePadding>
								<ListItemButton>
									<ListItemIcon>
										<Help />
									</ListItemIcon>
									<ListItemText primary="Help Centre" />
								</ListItemButton>
							</ListItem>
							<ListItem disablePadding>
								<ListItemButton>
									<ListItemIcon>
										<Box sx={{ overflow: 'hidden', borderRadius: '5px', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
											<img
												src="https://ui-avatars.com/api/?size=30&background=8660F1&color=fff&font-size=0.6&name=Jasmin+Finn"
												alt=""
												srcSet=""
											/>
										</Box>
									</ListItemIcon>
									<ListItemText primary="Jasmin Finn" />
								</ListItemButton>
							</ListItem>
						</List>
					</nav>
				</Box>
			</Drawer>
		</Box>
	);
};

export default NavDrawer;
