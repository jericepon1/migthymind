import React from "react";
import { makeStyles } from "@material-ui/core";
import { Nav } from "./Nav";
import { HeaderBanner } from "./HeaderBanner";
import Books from "../Images/Books.jpg";

const useStyles = makeStyles(({ palette, breakpoints }) => ({
	Header: {
		color: palette.common.white,
		backgroundColor: palette.primary.dark,
		[breakpoints.up("laptop")]: {
			background: `url(${Books}) no-repeat 4.2% 100%, ${palette.primary.dark}`,
		},
	},
}));

export const Header = () => {
	const classes = useStyles();

	return (
		<header className={classes.Header}>
			<Nav />
			<HeaderBanner />
		</header>
	);
};
