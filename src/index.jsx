import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { App } from "./App";
import * as serviceWorkerRegistration from "./serviceWorkerRegistration";
import reportWebVitals from "./reportWebVitals";
import { createTheme, ThemeProvider } from "@material-ui/core";

let theme = createTheme({
	palette: {
		primary: {
			main: "#3972F5",
			dark: "#0F2551",
		},
		secondary: {
			main: "#edf2ff",
			light: "#EF3988",
			dark: "#6740D2",
		},
		common: {
			muted: "#9FABC4",
		},
	},
	typography: {
		fontFamily: "Roboto",
		h4: {
			fontSize: "1.8125rem",
		},
	},
	breakpoints: {
		values: {
			mobile: 0,
			tablet: 992,
			laptop: 1024,
			desktop: 1280,
		},
	},
	overrides: {
		MuiButton: {
			root: {
				textTransform: "initial",
			},
		},
	},
});

ReactDOM.render(
	<ThemeProvider theme={theme}>
		<App />
	</ThemeProvider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
